﻿using System.Web;
using System.Web.Optimization;

namespace NetFlask
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                //~/Content/... is the convention to create StyleBundle
                  //the order of including bundle is important, need to follow the same as in original files(html/cshtml)

                  "~/Content/bootstrap.css", 
                  "~/css/style.css"));

            bundles.Add(new StyleBundle("~/Content/magnificpopup").Include(
               "~/css/popuo-box.css"

               ));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include
                (//~/bundles/... is the convention to create ScriptBundle
                    "~/Scripts/jquery-{version}.js"
                ));
            //cuz the 2 src scripts in index are at different places, so need to create another bundle.
            bundles.Add(new ScriptBundle("~/bundles/JqueryFlexiSel").Include
                (
                 "~/Js/jquery.flexisel.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/magnificpopup").Include
              (
               "~/js/jquery.magnific-popup.js"
              ));


        }
    }
}
