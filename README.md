

> Netflask-.net framework MVC exercise:
-  Etape 1 : Copier tout le code html du fichier index.html dans _layout.cshtml
- Etape 2 : Ajouter à la fin du fichier _layout.cshtml ==> @RenderBody() pour éviter une erreur asp
- Etape 3 : vérifier les version bootstrap et jquery et mettre à jour via le nuget manager celle de Visual studio
- Etape 4 : Créer les dossier js, css, images et y déplacer les fichiers qui ne sont pas des librairies
- Etape 5 : Vérifier le rendu sur le navigateur et adapter les chemins si nécessaires pour les images et fichiers js, css
- Etape 6 : Créer des bundles pour les fichiers css et js
- Etape 7 : Déplacer l'html qui correspond à la page home dans Home.cshtml
- Etape 8 : déplacer le renderBody() au bon endroit dans le layout


